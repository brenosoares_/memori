var gulp = require('gulp');  
var sass = require('gulp-sass');  
var sassGlob = require('gulp-sass-glob');
var browserSync = require('browser-sync');


//SASS
gulp.task('sass', function () {  
    gulp.src('sass/main.sass')
        .pipe(sassGlob())
        .pipe(sass({includePaths: ['sass']}))
        .pipe(gulp.dest('template/css'));
});

//BROWSERSYNC
gulp.task('browser-sync', function() {  
    browserSync.init(["css/*.css", "js/*.js"], {
        server: {
            baseDir: "./template"
        }
    });
});

//WATCH
gulp.task('default', ['sass', 'browser-sync'], function () {  
    gulp.watch("sass/**/*.sass", ['sass']).on("change", browserSync.reload);
    gulp.watch("template/**/*.html").on("change", browserSync.reload);
});