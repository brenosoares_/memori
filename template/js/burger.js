$(document).ready(function () {
	// Burger
	var menuButton = $('#menuButton');

	menuButton.on('click', function () {
		menuButton.toggleClass('is-active');
		$('.menu').toggleClass('is-active');
	});
});